FROM alpine:3.8

LABEL maintainer="Anucha Nualsi <ana.cpe9@gmail.com>"

COPY nginx-src /nginx-src

RUN chmod a+x /nginx-src/nginx-tools/gzip_static.sh && \
    chmod a+x /nginx-src/nginx-tools/install.sh && \
    chmod a+x /nginx-src/nginx-tools/uninstall.sh && \
    chmod a+x /nginx-src/nginx-tools/reinstall.sh && \
    /nginx-src/nginx-tools/gzip_static.sh

CMD ["exec", "$@"]
