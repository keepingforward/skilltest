# Project for SkillLane Test Package for Developer

---

## Init commands

```shell
ng new skilltest \
   --style scss \
   --prefix cha \
   --routing \
   --skip-git
```

```shell
npm install -S bootstrap font-awesome @fortawesome/fontawesome-free-webfonts
npm install -S @angular/material @angular/cdk @angular/animations material-design-icons-iconfont
npm install -S jwt-decode
ng add @angular/pwa
```

```shell
npm install -D karma-firefox-launcher uglify-es nginxbeautify
npm install -D body-parser jsonwebtoken express-jwt cors
npm install -D nodemon
```

## dev-256-bit-secret

Used ***256-bit WEP Keys*** by `randomkeygen.com`

dev >> `1C32A2BD35995AAEBF34B5D2FD75D`

prod > `A54B9AB76F4A5AFFFD29D6EEFDD84`

### Users, JWT

Header

```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```

VERIFY SIGNATURE

```text
HMACSHA256(
  base64UrlEncode(header) + "." +
  base64UrlEncode(payload),
  1C32A2BD35995AAEBF34B5D2FD75D
)
```

---

user: cha

token: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiY2hhIiwiZGlzcGxheSI6IkFudWNoYSBOdWFsc2kiLCJyb2xlIjoic3R1ZGVudCIsImlhdCI6MTUxNjIzOTAyMiwiZXhwaXJlZCI6MTUxNjIzOTAyMiwic2FsdCI6Ikc4WTVGSjM2ZEUifQ.C2Sqv5CzxlzSsoVHeGBZod5ckJrnDey7-EU0duq_QGc`

```json
{
  "user": "cha",
  "display": "Anucha Nualsi",
  "role": "student",
  "iat": 1516239022,
  "expired": 1516239022,
  "salt": "G8Y5FJ36dE"
}
```

---

user: teacher01

token: `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoidGVhY2hlcjAxIiwiZGlzcGxheSI6IlRlYWNoZXIwMSBBIiwicm9sZSI6Imluc3RydWN0b3IiLCJpYXQiOjE1MTYyMzkwMjIsImV4cGlyZWQiOjE1MTYyMzkwMjIsInNhbHQiOiJHOFk1RkozNmRFIn0.HSrO6qYa8hqlRoTfdANsPemDU4r5TX5aJZR1qILRxO8`

```json
{
  "user": "teacher01",
  "display": "Teacher01 A",
  "role": "instructor",
  "iat": 1516239022,
  "expired": 1516239022,
  "salt": "G8Y5FJ36dE"
}
```

## run

```shell
http-server --cors='*' -c-1 -a 0.0.0.0 -p 8088 ./dist/skilltest/
```

## Tools

- [jwt.io](https://jwt.io/)
- [RandomKeygen - The Secure Password & Keygen Generator](https://randomkeygen.com/)
- [List of HTTP status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
- [*Using Typescript with Express*](https://dev.to/briandgls/using-typescript-with-express--e0k)
