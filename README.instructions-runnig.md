# ตัวอย่าง

## Running the NodeJS + MongoDB API Locally

## Demo project
https://ski3l.keepingforward.com/
https://skilltest.keepingforward.com/

## เตรียมสภาพแวดล้อม

1. ติดตั้ง NodeJS และ NPM จาก  https://nodejs.org/en/download/.
2. ติดตั้ง MongoDB Community Server จาก  https://www.mongodb.com/download-center.

## Build
`git clone https://gitlab.com/keepingforward/skilltest.git`
`cd skilltest`
`npm install @angular/cli@6.2.1 -g`
`npm install http-server -g`
`npm install`
`ng run skilltest:build:production-local`
`node _edit_index.js`
`npm run ngsw-config`
`node _ugilfy_service-worker.js`
`node _cha_server_push_gen.js`

### รัน backend-api
`npm run jwt-server:dev`

### รัน frontend
`http-server --cors='*' -c-1 -a 0.0.0.0 -p 8088 ./dist/skilltest/`


```bash
docker run -d \
       --name mongodb \
       --restart always \
       --network bridge \
       -p 27017:27017 \
       -v /var/mongo/mydatabase:/data/db \
       mvertes/alpine-mongo
```


`http-server --cors='*' -c-1 -a 0.0.0.0 -p 8088 ./dist/skilltest/`
