# Project for SkillLane Test Package for Developer

---

## Quick start

- [Download MongoDB Community](https://www.mongodb.com/download-center?jmp=tutorials#community)
- [Install MongoDB Community - Windows](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)
- [TypeScript: Declaring Mongoose Schema + Model](https://brianflove.com/2016/10/04/typescript-declaring-mongoose-schema-model/)
  - [TypeScript 2 + Express + Mongoose + Mocha + Chai](https://brianflove.com/2016/11/11/typescript-2-express-mongoose-mocha-chai/)
- [*Using Typescript with Express*](https://dev.to/briandgls/using-typescript-with-express--e0k)

### Start / Stop the MongoDB service

`net start MongoDB`
> [initandlisten] waiting for connections on port 27017

`net stop MongoDB`

***Options***

`sc start MongoDB`
`sc query MongoDb`

### Remove MongoDB Community Edition as a Windows Service

`sc.exe delete MongoDB`

## Basic CRUD Operations

- **Create Operations**
  - db.collection.insertOne()
  - db.collection.insertMay()
- **Read Operations**
  - db.collection.find()
    - example: `db.users.find({ age: {$gt: 18} }, { name: 1, address: 1} ).limit(5)`
- **Update Operations**
  - db.collection.updateOne()
  - db.collection.updateMany()
    - example: `db.users.updateMany({ age: {$lt: 18 } }, { $set: { status: "reject" } })`
  - db.collection.replaceOne()
- **Delete Operations**
  - db.collection.deleteOne()
  - db.collection.deleteMany()
    - example: `db.users.deleteMany({ status: "reject" })`
- **Buld Write**
