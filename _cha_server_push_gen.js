'use strict';

// generate script to isolate production files, with http2_push mode.
// https://www.nginx.com/blog/nginx-1-13-9-http2-server-push/
// https://nginx.org/en/docs/http/ngx_http_v2_module.html#http2_max_concurrent_pushes

const _ngsw_path = './dist/skilltest/ngsw.json';
const _conf_path = './nginx-src.skilltest/nginx-container/conf.d/com.keepingforward.skilltest.conf';
const _conf_path_02 = './nginx-src.ski3l/nginx-container/conf.d/com.keepingforward.ski3l.conf';
let _ngsw_data = require(_ngsw_path);

const fs = require('fs');
// const NgxBeautify = require('nginxbeautify');

// assetGroups --> .name": "app"
var asset = _ngsw_data.assetGroups[0];

// ensure data path
if (asset && asset.name && asset.name === 'app') {
  console.log(asset.urls);
  console.log();

  var index = asset.urls.indexOf("/index.html");
  if (index > -1) {
    asset.urls.splice(index, 1);
  }

  console.log();
  console.log(asset.urls.reverse());
  console.log();

  var http2PushLocation = asset.urls[0];

  var hints_arr = asset.urls.map(function (str) {
    if (str.endsWith(".ico")) {
      return "add_header Link \"<".concat(str).concat("> as=image; rel=preload; nopush\";");
    } else if (str.endsWith(".css")) {
      return "add_header Link \"<".concat(str).concat("> as=style; rel=preload; nopush\";");
    } else if (str.endsWith(".js")) {
      return "add_header Link \"<".concat(str).concat("> as=script; rel=preload; nopush\";");
    }
  });

  if (hints_arr.length > 10) {
    console.log(`####################################\nhttp2_push list length is ${hints_arr.length},\nOver default maximum number of concurrent push requests.\n####################################\n`);
  }

  var linkHints = hints_arr.join('\n');

  asset.urls.splice(0, 1);

  var http2push = `location ${http2PushLocation}\n` +
    "{\n" +
    "expires 1d;\n" +
    "gzip_static on;\n" +
    "etag on;\n" +
    "add_header Cache-Control \"public\";\n\n" +
    "add_header X-Cha cha;\n\n" +
    asset.urls.map(function (str) {
      return "http2_push ".concat(str).concat(";");
    }).join('\n') +
    "\n}\n";

  console.log("-http2push-");
  console.log(http2push);
  console.log();
  console.log("-linkHints-");
  console.log(linkHints);
  console.log();

  var paths = [
    _conf_path,
    _conf_path_02,
  ];

  for (var i = 0; i < paths.length; i++) {
    var currentPath = paths[i];

    var data = fs.readFileSync(currentPath, 'utf8');


    // https push, generated for .css
    var fistResult = data.replace(/###_########## HTTP2_PUSH_GENERATED ##########_###/g, http2push);

    // replace http2_push list to placeholder
    var result = fistResult.replace(/###_########## LINK_HINTS_PLACEHOLDER ##########_###/g, linkHints);

    // // let instance = new NgxBeautify({
    // //   // tabs: 1
    // //   space: 4
    // // });

    // // // console.log(instance.parse(result));
    // // result = instance.parse(result);

    fs.writeFileSync(currentPath, result, 'utf8', function (err) {
      if (err) return console.log(err);

      console.log("Done: ".concat(currentPath));
    });
  }
} else {
  console.log("Error: unknown");
  process.exit(1);
}
