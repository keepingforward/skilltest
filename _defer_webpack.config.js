// npm install --save-dev html-webpack-plugin
const HtmlWebpackPlugin = require('html-webpack-plugin');

// // npm install --save-dev interpolate-html-plugin
// const InterpolateHtmlPlugin = require('interpolate-html-plugin');

// // npm install --save-dev preload-webpack-plugin
// const PreloadWebpackPlugin = require('preload-webpack-plugin');

// // npm install --save-dev resource-hints-webpack-plugin
// const ResourceHintWebpackPlugin = require('resource-hints-webpack-plugin');

// npm install --save-dev script-ext-html-webpack-plugin
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');

module.exports = {
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      // chunks: [
      //   'main',
      //   'polyfills',
      //   'runtime',
      //   'scripts',
      // ]
    }),
    // new InterpolateHtmlPlugin({
    //   'NODE_ENV': 'production'
    // }),
    // new PreloadWebpackPlugin(),
    // new PreloadWebpackPlugin({
    //   rel: 'preload',
    //   // as(entry) {
    //   //   if (/\.css$/.test(entry)) return 'style';
    //   //   return 'script';
    //   // }
    // }),
    // new ResourceHintWebpackPlugin(),
    new ScriptExtHtmlWebpackPlugin({
      defaultAttribute: 'defer'
    })
  ]
}
