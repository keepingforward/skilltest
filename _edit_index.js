const _indexHtml_path = './dist/skilltest/index.html';
const htmlMinify = require('html-minifier').minify;

const fs = require('fs');

fs.readFile(_indexHtml_path, 'utf8', function (err, data) {
  if (err) {
    return console.log(err);
  }

  var tmpdata = data;
  var styleIndex = data.indexOf("<link rel=\"stylesheet\" ");
  if (styleIndex > -1) {
    var styleEndIndex = data.indexOf('>', styleIndex + 1);

    // console.log(`styleIndex:    ${styleIndex}`);
    // console.log(`styleEndIndex: ${styleEndIndex}`);
    console.log(data.substr(styleIndex, styleEndIndex - styleIndex + 2));

    // media="none" onload="this.media='all'">
    var setMedia = " media=\"none\" onload=\"this.media='all'\"";
    tmpdata =
      data.substr(0, styleEndIndex)
      .concat(setMedia)
      .concat(data.substr(styleEndIndex));

    console.log(tmpdata.substr(styleIndex, (styleEndIndex - styleIndex) + setMedia.length + 2));
    console.log();
  }

  var scriptIndex = tmpdata.indexOf("</cha-root>");
  if (scriptIndex > -1) {
    var tailString = tmpdata.substr(scriptIndex);

    // console.log();
    // console.log(tailString);

    tailString = tailString.replace(/.js"><\/script>/g, ".js\" defer></script>");
    // console.log();
    // console.log(tailString);

    var headString = tmpdata.substr(0, scriptIndex);
    tmpdata = headString.concat(tailString);
    // console.log();
    // console.log(tmpdata);

    // https://github.com/kangax/html-minifier
    var finalResult = htmlMinify(tmpdata, {
      'html5': true,
      'removeAttributeQuotes': false,
      'collapseWhitespace': true,
      'minifyCSS': true,
      'minifyJS': true,
      'removeComments': true,
      'removeScriptTypeAttributes': true,
    });
    // console.log();
    console.log(finalResult);

    // write html-minified back.
    fs.writeFile(_indexHtml_path, finalResult, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  }
});
