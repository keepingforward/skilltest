ng build --progress=false && \
ng build --aot --progress=false && \
ng build --prod --progress=false && \
ng test --watch=false --progress=false --browsers=ChromeCI,FirefoxHeadless && \
ng e2e
