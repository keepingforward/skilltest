const minify = require('uglify-es').minify;
const fs = require('fs');

var baseDir = './dist/skilltest/';

// https://skalman.github.io/UglifyJS-online/
var uglifyConfig = {
  compress: {
    ie8: false,
    sequences: false,
    keep_classnames: true,
    keep_fargs: true,
    keep_fnames: true,
    keep_infinity: true,
  },
  mangle: false,
  output: {
    ie8: false,
    beautify: true,
    // ascii_only: true,
  },
};

// console.log(baseDir.concat('worker-basic.min.js'));
// var data = fs.readFileSync(baseDir.concat('worker-basic.min.js'), 'utf8');
// console.log(minify(data, uglifyConfig).code);
var files = [
  'worker-basic.min.js',
  'safety-worker.js',
  'ngsw-worker.js',
];

for (var i = 0; i < files.length; i++) {
  var filePath = baseDir.concat(files[i]);
  var rawData = fs.readFileSync(filePath, 'utf8');
  var minifyTxt = minify(rawData, uglifyConfig).code;
  fs.writeFileSync(filePath, minifyTxt, 'utf8');
  console.log(`${filePath}, ${rawData.length} to ${minifyTxt.length}`);
}

// var filePath = baseDir.concat('worker-basic.min.js');
// var rawData = fs.readFileSync(filePath, 'utf8');
// var minifyTxt = minify(rawData, uglifyConfig).code;
// fs.writeFileSync(filePath, minifyTxt, 'utf8');
// console.log(`${filePath}, ${rawData.length} to ${minifyTxt.length}`);

// filePath = baseDir.concat('safety-worker.js');
// rawData = fs.readFileSync(filePath, 'utf8');
// minifyTxt = minify(rawData, uglifyConfig).code;
// fs.writeFileSync(filePath, minifyTxt, 'utf8');
// console.log(`${filePath}, ${rawData.length} to ${minifyTxt.length}`);

// filePath = baseDir.concat('ngsw-worker.js');
// rawData = fs.readFileSync(filePath, 'utf8');
// minifyTxt = minify(rawData, uglifyConfig).code;
// fs.writeFileSync(filePath, minifyTxt, 'utf8');
// console.log(`${filePath}, ${rawData.length} to ${minifyTxt.length}`);
