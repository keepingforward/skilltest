import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  // https://github.com/angular/protractor/issues/2963#issuecomment-372187210
  let originalTimeout;
  beforeEach(function () {
    originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

    page = new AppPage();
  });
  afterEach(function () {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = originalTimeout;
  });

  // beforeEach(() => {
  //   page = new AppPage();
  // });

  it('should display welcome message', () => {
    page.navigateTo();
    // expect(page.getParagraphText()).toEqual('ลงชื่อเข้าใช้งาน');
    expect(page.getParagraphText()).toEqual('Skillane Course.');
  });
});
