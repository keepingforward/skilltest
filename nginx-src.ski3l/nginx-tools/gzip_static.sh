#!/bin/ash

#http://wiki.linuxwall.info/doku.php/en:ressources:dossiers:nginx:nginx_performance_tuning
FILETYPES="*.html *.woff *.woff2 *.ttf *.eot *.css *.jpg *.jpeg *.gif *.png *.ico *.js *.txt *.json *.svg *.webmanifest *.xml"

#DIRECTORIES="/usr/share/nginx/html/"
DIRECTORIES="/nginx-src/nginx-container/html/"

for currentdir in $DIRECTORIES
do
   for i in $FILETYPES
   do
      find $currentdir -iname "$i" -exec ash -c 'PLAINFILE={};GZIPPEDFILE={}.gz; \
         if [ -e $GZIPPEDFILE ]; \
         then   if [ `stat -c %Y $PLAINFILE` -gt `stat -c %Y $GZIPPEDFILE` ]; \
                then    echo "$GZIPPEDFILE outdated, regenerating"; \
                        gzip -9 -f -c $PLAINFILE > $GZIPPEDFILE; \
                 fi; \
         else echo "$GZIPPEDFILE is missing, creating it"; \
              gzip -9 -c $PLAINFILE > $GZIPPEDFILE; \
         fi' \;

######################
## #### Brotli #### ##
## apk add brotli --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/
######################
#      find $currentdir -iname "$i" -exec ash -c 'PLAINFILE={};BROTLIFILE={}.br; \
#          if [ -e $BROTLIFILE ]; \
#          then   if [ `stat -c %Y $PLAINFILE` -gt `stat -c %Y $BROTLIFILE` ]; \
#                 then    echo "$BROTLIFILE outdated, regenerating"; \
#                         brotli -9 -f -c $PLAINFILE > $BROTLIFILE; \
#                  fi; \
#          else echo "$BROTLIFILE is missing, creating it"; \
#               brotli -9 -c $PLAINFILE > $BROTLIFILE; \
#          fi' \;
  done
done
