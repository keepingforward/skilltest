#!/bin/sh

set -e

echo "remove old files ..."
rm -f /var/nginx/conf.d/com.keepingforward.ski3l.conf
rm -f /var/nginx/conf.d/ssl/ssl-com.keepingforward.ski3l.conf
rm -rf /var/nginx/html/com.keepingforward.ski3l/
echo "remove old files done."
