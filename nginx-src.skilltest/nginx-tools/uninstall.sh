#!/bin/sh

set -e

echo "remove old files ..."
rm -f /var/nginx/conf.d/com.keepingforward.skilltest.conf
rm -f /var/nginx/conf.d/ssl/ssl-com.keepingforward.skilltest.conf
rm -rf /var/nginx/html/com.keepingforward.skilltest/
echo "remove old files done."
