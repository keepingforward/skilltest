module.exports = errorHandler;

function errorHandler(err, req, res, next) {
  if (typeof (err) === 'string') {

    console.error('400: ' + err);

    return res.status(400).json({
      message: err
    });
  }

  if (err.name === 'ValidationError') {

    console.error('400: ' + err);

    return res.status(400).json({
      message: err.message
    });
  }

  if (err.name === 'UnauthorizedError') {

    console.error('401: ' + err);

    return res.status(401).json({
      message: 'Invalid Token'
    });
  }

  console.error('500: ' + err.message);

  // default to 500 server error
  return res.status(500).json({
    message: err.message
  });
}
