// ###########################################################################################
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/cha_db', {
  // mongoose.connect('mongodb://localhost:27017/test-should-error', {
  useNewUrlParser: true
});
mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on('connected', function () {
  console.log("Mongoose default connection is open to ");
});

db.on('error', function (err) {
  console.log("Mongoose default connection has occured " + err + " error");
});

db.on('disconnected', function () {
  console.log("Mongoose default connection is disconnected");
});

process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log("Mongoose default connection is disconnected due to application termination");
    process.exit(0);
  });
});

// ###########################################################################################
// https://mongoosejs.com/docs/schematypes.html
var Schema = mongoose.Schema;
var UserModelSchema = new Schema({
  _id: Schema.Types.ObjectId,
  username: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    index: true,
    unique: true
  },
  role: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    index: true,
    enum: ['student', 'instructor']
  },
  firstname: String,
  lastname: String,
  nickname: String,
  gender: {
    type: String,
    trim: true,
    enum: ['male', 'female']
  },
  birthday: String,
  password: {
    type: String, // ตอนนี้ใช้ plain text ไปก่อนละกัน
    required: true
  }
});

var CourseModelSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
    trim: true
  },
  author: {
    type: UserModelSchema,
    required: true,
  },
  description: String,
  category: String,
  subject: String,
  timestart: Date,
  timeend: Date,
  numberofstudent: {
    type: Number,
    required: true,
    min: 0
  },
  students: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }],
  // students2: [
  //   UserModelSchema
  // ]
});

// ###########################################################################################
var UserModel = mongoose.model('User', UserModelSchema);
var CourseModel = mongoose.model('Course', CourseModelSchema);


// ###########################################################################################
var userNames = ['user01', 'user02', 'user03', 'user04', 'user05', 'user06', 'user07'];
var userLen = userNames.length;

async function genUsers() {
  for (var i = 0; i < userLen; i++) {
    var userName = userNames[i];

    await UserModel
      .findOne({
        'username': userName
      })
      .then(_user => {
        if (_user) {
          // users[i] = user;
          console.log(`genUsers: ${userName} founded`);
        } else {
          var tmpUser = new UserModel({
            _id: new mongoose.Types.ObjectId(),
            username: userName,
            role: 'instructor',
            firstname: 'firstname-' + userName,
            lastname: 'lastname-' + userName,
            nickname: 'nickName-' + userName,
            gender: 'male',
            birthday: '15/05/2524',
            password: '' + userName,
          });

          tmpUser.save();
          console.log(`genUsers: ${userName} created`);
        }
      })
      .catch(err => {
        console.error(`genUsers:error: ${userName} adding`);
        console.error(`genUsers:error: ${error}`);
      });
  }
};

async function listAllUsers() {
  await UserModel
    .find()
    .then(users => {
      users.forEach(foundedUser => {
        console.log(`listAllUsers: found "${foundedUser.username}"`);
      })
    })
    .catch(error => {
      console.error('listAllUsers:error: list all users');
      console.error(`listAllUsers:error: ${error}`);
      throw error;
    });
}

async function getUser(username) {

  console.warn('+getUser: ' + username);

  return await UserModel
    .findOne({
      'username': username
    })
  // .then(user => {
  //   if (!user)
  //     console.warn('getUser:Warn: ไม่พบ user: ' + username);

  //   console.log('+getUser:user: ' + user);

  //   return user;
  // })
  // .catch(error => {
  //   console.error(`getUser:error: get user by username: "${username}"`);
  //   console.error(`getUser:error: ${error}`);
  //   throw error;
  // });
}

// ###########################################################################################
console.log();
console.log('genUsers();');
genUsers()
  .then(() => {
    console.log('_:genUsers was success');
  }).catch(error => {
    console.log(`_:genUsers:error ${error}`);
  });

console.log();
console.log('listAllUsers();');
listAllUsers()
  .then(() => {
    console.log('_:listAllUsers was success');
  })
  .catch(error => {
    console.log(`_:listAllUsers:error ${error}`);
  });

console.log();
console.log('getUser(\'user01\')');
getUser('user01')
  .then(user => {
    if (user) {
      console.log(`_:getUser is ${user}`);
    } else {
      console.warn(`_:getUser was not founded`);
    }
  }).catch(error => {
    console.log(`_:getUser:error ${error}`);
  });

// ###########################################################################################
var courseNames = ['01: Course Name abcd', '02: Course Name efgh', '03: Course Name jklm', '04: Course Name nopq', '05: Course Name rstu', '06: Course Name wuxy', '07: Course Name zzzz'];
var courseDates = [new Date('2018-09-01'), new Date('2018-09-02'), new Date('2018-09-03'), new Date('2018-09-04'), new Date('2018-09-05'), new Date('2018-09-06'), new Date('2018-09-07'), new Date('2018-09-08')];
var courseLen = courseNames.length;

async function genCourses() {
  for (var i = 0; i < courseLen; i++) {
    var courseName = courseNames[i];
    var userName = 'user' + courseName.substr(0, 2);

    // console.log(`+genCourses:log: userName is "${userName}"`);

    await CourseModel
      .findOne({
        'name': courseName
      })
      .then(async (_course) => {
        if (_course) {
          // users[i] = user;
          console.log(`course '${_course.name}' was founded`);
        } else {

          var user = await getUser(userName);
          console.log(`######################################\n${user}\n######################################\n`)

          if (!user) {
            console.error(`genCourses:error: ${user} not founded`);
            throw 'Error:genCourses: ไม่พบ username: "' + userName + '"';
          }

          var tmpCourse = new CourseModel({
            _id: new mongoose.Types.ObjectId(),
            name: courseName,
            author: user,
            description: '',
            category: '',
            subject: '',
            timestart: courseDates[i], // new Date(),
            timeend: courseDates[i + 1], // Fnew Date((new Date()) + 30 * 60000), // บวก 30 นาที
            numberofstudent: 1,
            students: [user],
            // students2: [user]
          });

          tmpCourse.save();
          console.log(`'${courseName}' was created`);
        }
      })
      .catch(err => {
        console.error(`error: ${courseName} adding`);
        console.error(error);
      });
  }
};

async function listAllCourses() {
  await CourseModel
    .find()
    .then(courses => {
      if (courses) {
        courses.forEach(course => {
          console.log(`listAllCourses: course '${course.name}' was found`);
        })
      } else {
        console.warn(`listAllCourses: was not founded`);
      }
    })
    .catch(error => {
      console.error('listAllCourses:error: list all courses');
      console.error(`listAllCourses:error: ${error}`);
    });
}

// CourseModel.findOne({}, {})

console.log();
console.log('genCourses();');
genCourses()
  .then(() => {
    console.log('_:genCourses: was success');
  })
  .catch(error => {
    console.error(`_:genCourses:error: ${error}`);
  });

console.log();
console.log('listAllCourses();');
listAllCourses()
  .then(() => {
    console.log('_:listAllCourses: was success');
  })
  .catch(error => {
    console.error(`_:listAllCourses:error: ${error}`);
  });
