// https://expressjs.com/en/4x/api.html#app.mountpath

var express = require('express');

var web = express(); // the main app
var admin = express(); // the sub app
var _port = 4222;



web.use('/api/v1', admin);

admin.get('/users', function (req, res) {
  console.log(req.url);
  console.log(admin.mountpath);
  res.send('User Homepage');
});

admin.get('/users/auth', function (req, res) {
  console.log(req.url);
  console.log(admin.mountpath);
  res.send('Auth Homepage');
});

web.listen(_port, function () {
  console.log(`Server listening on port ${_port}!`);
});
