var jwt = require('jsonwebtoken');

module.exports = function (headers) {
  var payload = null;

  if (
    headers.authorization &&
    headers.authorization.split(' ')[0] === 'Bearer'
  ) {
    try {
      let token = headers.authorization.split(' ')[1];
      payload = jwt.decode(token);
    } catch (err) {
      throw 'decode token error';
    }
  }

  return payload;
}
