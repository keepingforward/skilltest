// ###########################################################################################
// Express User's Router module
// ###########################################################################################
const router = require('express').Router();

router.get('/', getAll);
router.get('/registered', getRegisteredCourses);
router.get('/mind', getMyCourses);
router.get('/name/:name', searchByName);
router.get('/time/:time', searchByTime);
router.get('/detail/:id', getById);
router.post('/', creatCourse);
router.put('/', updateCourse);
router.post('/registered', updateRegisterCourse);

module.exports = router;

// ###########################################################################################
// Course's Controllers
// ###########################################################################################
const courseService = require('./course.service');
const decodeJwtToken = require('./_decodeJwtToken');

function getAll(req, res, next) {
  courseService.getAll()
    .then(courses => res.json(courses))
    .catch(err => next(err));
}

function getById(req, res, next) {
  courseService.getById(req.params.id)
    .then(course => res.json(course))
    .catch(err => next(err));
}

function getRegisteredCourses(req, res, next) {

  let tokenPayload = decodeJwtToken(req.headers);

  courseService.getRegisteredCourses(tokenPayload._id)
    .then(courses => res.json(courses))
    .catch(err => next(err));
}

function getMyCourses(req, res, next) {

  let tokenPayload = decodeJwtToken(req.headers);

  courseService.getMyCourses(tokenPayload._id)
    .then(courses => res.json(courses))
    .catch(err => next(err));
}

function searchByName(req, res, next) {
  courseService.searchByName(req.params.name)
    .then(courses => res.json(courses))
    .catch(err => next(err));
}

function searchByTime(req, res, next) {

  // console.log(req.params.time);

  var dateStr = JSON.parse(req.params.time);
  console.log(dateStr);

  var time = new Date(dateStr);

  courseService.searchByTime(time)
    .then(courses => res.json(courses))
    .catch(err => next(err));
}

function creatCourse(req, res, next) {

  let tokenPayload = decodeJwtToken(req.headers);

  courseService.create(tokenPayload._id, req.body)
    .then(() => res.status(201).json({}))
    .catch(err => next(err));
}

function updateCourse(req, res, next) {

  let tokenPayload = decodeJwtToken(req.headers);

  courseService.update(tokenPayload._id, req.body)
    .then(() => res.status(202).json({}))
    .catch(err => next(err));
}

function updateRegisterCourse(req, res, next) {

  let tokenPayload = decodeJwtToken(req.headers);

  courseService.updateRegisterCourse(tokenPayload._id, req.body)
    .then(() => res.status(202).json({}))
    .catch(err => next(err));
}
