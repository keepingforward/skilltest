// ###########################################################################################
// Config mongoose course's schema
// https://mongoosejs.com/docs/schematypes.html
// ###########################################################################################
const mongoose = require('mongoose');
const userService = require('./user.service');
const UserModelSchema = userService.UserModelSchema;
const userModel = userService.userModel;

const Schema = mongoose.Schema;
const CourseModelSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: {
    type: String,
    required: true,
    trim: true
  },
  author: {
    type: UserModelSchema, // instructor
    required: true,
  },
  description: String,
  category: String,
  subject: String,
  timestart: Date,
  timeend: Date,
  numberofstudent: {
    type: Number,
    required: true,
    min: 0
  },
  students: [{
    type: Schema.Types.ObjectId,
    ref: 'User'
  }]
});

CourseModelSchema.index({
  name: 'text'
});

const courseModel = mongoose.model('Course', CourseModelSchema);
// courseModel.index.

console.log('======================================================');
console.log(userModel.schema.indexes())
console.log(courseModel.schema.indexes());
console.log('======================================================');

courseModel.collection.getIndexes({
  full: true
}, (error, indexes) => {
  console.log();

  if (error) {
    console.log('Error: courseModel.collection.getIndexes');
    console.log(error);
    return;
  }

  console.log(indexes);
  courseModel.collection.indexExists('author.username_1', (error, found) => {
    if (error) {
      console.log('courseModel.collection.getIndexes');
      console.log(error);
      return;
    }

    if (!found) {
      // console.log('Index name "author.username_1", is not found');
      return;
    }

    courseModel.collection.dropIndex('author.username_1', (drop_err, drop_resp) => {
      console.log('------------------------------------');
      console.log(drop_err);
      console.log(drop_resp);
      console.log('------------------------------------');

      // list indexes again.
      courseModel.collection.getIndexes({
        full: true
      }, (err_again, after_indexes) => {
        console.log('#####################################');
        console.log(after_indexes);
        console.log('#####################################');
      });
    });

    // courseModel.collection.ensureIndex({
    //   'author.username': 1
    // }, {
    //   unique: false
    // }, (error_1, result) => {
    //   console.log();
    //   console.log('+++++++++++++++++++++++++++++++++++++++++++++++');
    //   console.log(error_1);
    //   console.log(result);
    //   console.log('+++++++++++++++++++++++++++++++++++++++++++++++');
  });
});


// ###########################################################################################
// Services
// ###########################################################################################
module.exports = {
  getAll,
  getById,
  getRegisteredCourses,
  getMyCourses,
  searchByName,
  searchByTime,
  create,
  update,
  // updateRegister,
  updateRegisterCourse
};

// ###########################################################################################
// Service's definition
// ###########################################################################################

// แสดงข้อมูลที่จำเป็นเท่านั้น
const selectProjection = {
  '_id': 1,
  'name': 1,
  'subject': 1,
  'category': 1,
  'description': 1,
  'author._id': 1,
  'author.username': 1,
  'author.firstname': 1,
  'author.lastname': 1,
  'timestart': 1,
  'timeend': 1,
  'numberofstudent': 1,
  'students': 1
}

async function getAll() {
  return await courseModel.find({}, selectProjection);
}

async function getById(courseId) {
  console.log(courseId);
  return await courseModel.findOne({
    "_id": new mongoose.Types.ObjectId(courseId)
  }, selectProjection);
}

async function getRegisteredCourses(studentUserId) {
  return await courseModel.find({
    "students": new mongoose.Types.ObjectId(studentUserId)
  }, selectProjection);
}

async function getMyCourses(authorUserId) {
  return await courseModel.find({
    "author._id": new mongoose.Types.ObjectId(authorUserId)
  }, selectProjection);
}

async function searchByName(searchCourseName) {

  // console.log(`searchByName: ${searchCourseName}`);

  return await courseModel.find({
      $text: {
        $search: `"${searchCourseName}"`
      }
    },
    selectProjection);
}

async function searchByTime(time) {

  // console.log(new Date());
  // console.log(time);

  return await courseModel.find({
    timestart: {
      '$gte': time,
      '$lte': time,
    },
    timeend: {
      '$gte': time,
      '$lte': time,
    }
  }, selectProjection);
}

async function update(authorUserId, newCourse) {

  console.log(update);
  console.log(newCourse);

  const oldCourse = await courseModel.findOne({
    _id: new mongoose.Types.ObjectId(newCourse._id),
    'author._id': new mongoose.Types.ObjectId(authorUserId),
  });

  if (!oldCourse) throw new Error('ไม่พบบทความนี้ตรงกับผู้สอน');

  const user = await userModel.findById(authorUserId);

  if (!user) {
    throw new Error('ไม่พบผู้ในระบบที่เพิ่มบทเรียนได้');
  }

  if (user.role !== 'instructor') {
    throw new Error('ไม่อนุญาติให้เพิ่มบทเรียนได้ นอกจากผู้ใช้ที่เป็น "ผู้สอน" เท่านั้น');
  }

  Object.assign(oldCourse, newCourse);
  oldCourse.author = user;

  await oldCourse.save();
};

async function create(authorUserId, newCourseRaw) {

  const user = await userModel.findById(authorUserId);

  if (!user) {
    throw new Error('ไม่พบผู้ในระบบที่เพิ่มบทเรียนได้');
  }

  if (user.role !== 'instructor') {
    throw new Error('ไม่อนุญาติให้เพิ่มบทเรียนได้ นอกจากผู้ใช้ที่เป็น "ผู้สอน" เท่านั้น');
  }

  console.log(newCourseRaw);

  const newCourse = new courseModel(newCourseRaw);
  newCourse._id = new mongoose.Types.ObjectId();
  newCourse.author = user;
  newCourse.numberofstudent = 0

  console.log(newCourse);

  await newCourse.save();
}

// async function updateRegister(currentUserId, courseTobeUpdateRaw) {

//   const oldCourse = courseModel.findById(courseTobeUpdateRaw._id);
//   if (!oldCourse) throw new Error('ไม่พบบทความนี้ตรงกับเอกสารใด ๆ');
//   Object.assign(oldCourse, courseTobeUpdateRaw);

//   await oldCourse.save();
// }

async function updateRegisterCourse(tokenId, body) {

  let currentUserId = body.studentid;
  let courseId = body.courseid;

  if (tokenId !== currentUserId) throw new Error('Token ไม่ตรงกัน');

  const oldCourse = await courseModel.findById(courseId);
  if (!oldCourse) throw new Error('ไม่พบบทความนี้ตรงกับเอกสารใด ๆ');

  // console.log(oldCourse._id);
  // console.log(oldCourse.students);
  // console.log(oldCourse);

  oldCourse.students.push(
    new mongoose.Types.ObjectId(currentUserId)
  );

  oldCourse.numberofstudent = oldCourse.students.length;

  await oldCourse.save();
}
