// ###########################################################################################
// Express User's Router modules
// ###########################################################################################
const router = require('express').Router();

router.get('/', getCurrentUser);
router.put('/', updateUser);
router.post('/', createUser);
router.post('/auth', authenticate);

module.exports = router;

// ###########################################################################################
// User's Controllers
// ###########################################################################################
const userService = require('./user.service');
const decodeJwtToken = require('./_decodeJwtToken');

function getCurrentUser(req, res, next) {

  let tokenPayload = decodeJwtToken(req.headers);

  userService.getById(tokenPayload._id)
    .then((user) => res.status(200).json(user))
    .catch(err => next(err));
}

function updateUser(req, res, next) {

  let tokenPayload = decodeJwtToken(req.headers);

  userService.update(tokenPayload._id, req.body)
    .then(() => res.status(202).json({}))
    .catch(err => next(err));
}

function createUser(req, res, next) {
  userService.create(req.body)
    .then(() => res.status(201).json({}))
    .catch(err => next(err));
}

function authenticate(req, res, next) {

  // console.log(req.body);

  userService.authenticate(req.body)
    .then(user => user ? res.json(user) : res.status(400).json({
      message: 'ชื่อผู้ใช้ หรือรหัสผ่านไม่ถูกต้อง'
    }))
    .catch(err => next(err));
}
