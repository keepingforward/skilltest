// ###########################################################################################
// Config mongoose user's schema
// https://mongoosejs.com/docs/schematypes.html
// ###########################################################################################
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UserModelSchema = new Schema({
  _id: Schema.Types.ObjectId,
  username: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    index: true,
    unique: true
  },
  role: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    index: true,
    enum: ['student', 'instructor']
  },
  firstname: String,
  lastname: String,
  nickname: String,
  gender: {
    type: String,
    trim: true,
    enum: ['', 'male', 'female']
  },
  birthday: String,
  password: {
    type: String, // ตอนนี้ใช้ plain text ไปก่อนละกัน
    required: true
  }
});

const userModel = mongoose.model('User', UserModelSchema);

// ###########################################################################################
// Services
// ###########################################################################################
module.exports = {
  UserModelSchema,
  userModel,

  update,
  create,
  authenticate,
  getById
};

// ###########################################################################################
// User Service's definition
// ###########################################################################################

// var USERS = require('../fake-database.json').users;
const _secretKey = require('../server.config.json').secretKey;
const jwt = require('jsonwebtoken');

async function getById(id) {
  const user = await userModel
    .findById(id)
    .select('username role firstname lastname nickname gender birthday');

  if (!user) throw 'ไม่พบผู้ใช้งานนี้ในระบบ';

  return user;
};

async function update(id, newUser) {
  const user = await userModel.findById(id);

  if (!user) throw 'ไม่พบผู้ใช้งานนี้ในระบบ';
  if (user.username !== newUser.username && await userModel.findOne({
      username: newUser.username
    })) {
    throw 'ชื่อผู้ใช้งาน "' + newUser.username + '", ถูกใช้แล้ว';
  }

  newUser.password = user.password;
  Object.assign(user, newUser);

  await user.save();
};

async function create(newUser) {

  if (await userModel.findOne({
      username: newUser.username
    })) {
    throw 'ชื่อผู้ใช้งาน "' + newUser.username + '", ถูกใช้แล้ว';
  }

  const user = new userModel(newUser);

  //   // ตอนนี้ไม่ต้องเข้ารหัส เพื่อความง่าย
  //   if (newUser.password) {
  //     user.password = bcrypt.hashSync(newUser.password, 10);
  //   }

  user._id = new mongoose.Types.ObjectId();
  await user.save();
};

async function authenticate({
  username,
  password
}) {

  // let hashPassword = bcrypt.hashSync(password, 10);
  let hashPassword = password;

  //   // // ใช้ Json ก่อนฐานข้อมูลจริง
  //   // var found = USERS.find(user =>
  //   //   user.username.toLowerCase() == body.username.toLowerCase() &&
  //   //   user.password == body.password
  //   // );

  const user = await userModel.findOne({
    'username': username.toLowerCase(),
    'password': hashPassword
  });

  if (user) {
    const token = jwt.sign({
        _id: user._id,
        username: user.username,
        role: user.role,
      },
      _secretKey, {
        expiresIn: '1w' // '48h'
      });

    console.log(token);

    return {
      'username': user.username,
      'token': token
    };
  }
}
