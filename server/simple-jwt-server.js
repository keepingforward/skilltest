// ###########################################################################################
// Config
// ###########################################################################################
const config = require('./server.config.json');
const _secretKey = config.secretKey;
const _baseApiHref = config.baseApiHref;
const _port = config.port;
const _mongoDb = config.mongoDbConnection;

// ###########################################################################################
// Set up mongoose connection
// ###########################################################################################
var mongoose = require('mongoose');
var mongoDB = process.env.MONGODB_URI || _mongoDb

mongoose.connect(mongoDB, {
  useNewUrlParser: true
});

mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on('error', function (err) {
  console.error.bind(console, 'Mongoose default connection has occured ' + err + ' error');
});

db.on('connected', function () {
  console.log("Mongoose default connection is open to ");
});

db.on('disconnected', function () {
  console.log("Mongoose default connection is disconnected");
});

process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log("Mongoose default connection is disconnected due to application termination");
    process.exit(0);
  });
});

// ###########################################################################################
// Set up express-webserver
// https://github.com/expressjs/cors#enabling-cors-pre-flight
// http://johnzhang.io/options-request-in-express
// 
// ###########################################################################################
// Cr. "Sebastian Schocke"
// https://github.com/sschocke/angular-jwt-todo/blob/master/server/app.js

const _ = require('lodash');
const cors = require('cors');
const express = require('express');
const expressJwt = require('express-jwt');
const bodyParser = require('body-parser');
const globalErrorHandler = require('./_error-handler');

const web = express();
const app = express();

var usersRouter = require('./routes/user.controller');
var coursesRouter = require('./routes/course.controller');

web.use(function (req, res, next) {
  res.contentType('application/json');
  next();
});
web.use(cors());
web.use(_baseApiHref, app);
web.get('/', function (req, res) {
  res.json('Angular Simple API JWT-Server');
});

app.use(bodyParser.json());
app.use(expressJwt({
  secret: _secretKey
}).unless({
  path: [
    '/',
    _baseApiHref,
    {
      url: _baseApiHref.concat('/users'),
      methods: ['POST', 'OPTIONS']
    },
    {
      url: _baseApiHref.concat('/users/auth'),
      methods: ['POST', 'OPTIONS']
    }
  ]
}));

app.get('/', function (req, res) {
  res.json('JWT-Server is online');
});
app.use('/users', usersRouter);
app.use('/courses', coursesRouter);

web.use(globalErrorHandler); // <---
web.listen(_port, function () {
  console.log(`Angular JWT Simple API Server listening on port: ${_port}`);
});
