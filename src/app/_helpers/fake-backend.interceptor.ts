import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize, debounceTime } from 'rxjs/operators';
import { UserTokenDto } from '../_model/user-token-dto';
const JSON_DB = require('../../../server/fake-database.json');

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor() {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // const testUser = {
        //     id: 1,
        //     username: 'test',
        //     password: 'test',
        //     firstName: 'Test',
        //     lastName: 'User'
        // };

        // const testUser = USERS[0];

        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {

            // authenticate
            if (request.url.endsWith('api/v1/users/auth') && request.method === 'POST') {

                const found = JSON_DB.users.find(function (user) {
                    return request.body.username.toLowerCase() === user.username.toLowerCase() && request.body.password === user.password;
                });

                console.log(`USERS.find: ${found}`);

                // if (request.body.username === testUser.username && request.body.password === testUser.password) {
                if (found) {

                    console.log(`username: ${found.username}`);
                    console.log(`firstName: ${found.firstName}`);

                    // if login details are valid return 200 OK with a fake jwt token
                    const body: UserTokenDto = {
                        // id: testUser.id,
                        username: found.username,
                        firstName: found.firstName,
                        lastName: found.lastName,
                        token: found.token,
                    };

                    return of(new HttpResponse({ status: 200, body }))
                        .pipe(
                            materialize()
                        ).pipe(
                            // จำลอง latency 1 วินาที
                            delay(1000)
                        ).pipe(
                            dematerialize()
                        );
                } else {
                    // else return 400 bad request
                    return throwError(
                        {
                            error: { message: 'Username or password is incorrect' },
                        }
                    ).pipe(
                        materialize()
                    ).pipe(
                        // จำลองการชลอการตอบกลับ
                        // ป้องกันการ bruteforce เบื้องต้น, 4 วินาที
                        delay(4 * 1000)
                    ).pipe(
                        dematerialize()
                    );
                }
            }

            if (request.url.endsWith('api/v1/users') && request.method === 'POST') {
                const found = JSON_DB.users.find(function (user) {
                    return request.body.username.toLowerCase() === user.username.toLowerCase();
                });

                if (found) {
                    return throwError({ error: { message: 'username exists.' } });
                }

                const new_id = JSON_DB.users.length + 101;
                JSON_DB.users.push(
                    {
                        'id': new_id,
                        'username': request.body.username.toLowerCase(),
                        'password': request.body.password,
                        'firstName': request.body.firstName,
                        'lastName': request.body.lastName,
                        'nickName': request.body.nickName,
                        'gender': request.body.gender,
                        'birthday': request.body.birthday,
                        'role': request.body.role,
                        'token': 'fake-jwt-' + request.body.username.toLowerCase()
                    }
                );

                return of(new HttpResponse({
                    status: 201
                }))
                    .pipe(
                        materialize()
                    ).pipe(
                        // จำลอง latency 1 วินาที
                        delay(1000)
                    ).pipe(
                        dematerialize()
                    );
            }

            // get users
            if (request.url.endsWith('api/v1/users') && request.method === 'GET') {

                const token = request.headers.get('Authorization');
                const found = JSON_DB.users.find(user => 'Bearer '.concat(user.token) === token);

                if (found) {
                    const projectedUser = {
                        username: found.username,
                        firstName: found.firstName,
                        lastName: found.lastName,
                        nickName: found.nickName,
                        gender: found.gender,
                        birthday: found.birthday,
                    };

                    return of(new HttpResponse({
                        status: 200, body: [
                            projectedUser
                        ]
                    }));
                } else {
                    return throwError({ error: { message: 'Unauthorised' } });
                }
            }

            // pass through any requests not handled above
            return next.handle(request);

        }))

            // call materialize and dematerialize to ensure delay
            // even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .pipe(materialize())
            .pipe(delay(2 * 100))
            .pipe(dematerialize());
    }
}
