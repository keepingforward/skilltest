import { UserFullDto } from './user-full-dto';

export interface CourseDto {
    _id: string;
    students: string[];
    name: string;
    author: UserFullDto;
    description?: string;
    category?: string;
    subject?: string;
    timestart?: Date;
    timeend?: Date;
    numberofstudent?: number;
}
