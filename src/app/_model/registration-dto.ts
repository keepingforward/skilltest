import { UserDto } from './user-dto';

export interface RegistrationDto extends UserDto {
    password: string;
}
