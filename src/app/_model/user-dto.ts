export interface UserDto {
    username: string;
    role: string;
    firstname?: string;
    lastname?: string;
    nickname?: string;
    gender?: string;
    birthday?: string;
}
