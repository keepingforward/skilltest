import { UserDto } from './user-dto';

export interface UserFullDto extends UserDto {
    _id: string;
}
