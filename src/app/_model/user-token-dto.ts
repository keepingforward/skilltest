export interface UserTokenDto {
    username: string;
    token: string;
    firstName?: string;
    lastName?: string;
}
