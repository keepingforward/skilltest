import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AppTitleService {
    private title$ = new Subject<string>();

    constructor() { }

    titleChanged(): Observable<string> {
        return this.title$.asObservable();
    }

    setTitle(newTitle: string) {
        this.title$.next(newTitle);
    }
}

