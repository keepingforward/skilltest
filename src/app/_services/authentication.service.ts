import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, debounceTime, mergeMap, mergeMapTo, first } from 'rxjs/operators';
import { UserTokenDto } from '../_model/user-token-dto';

import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private login$ = new Subject<UserTokenDto>();
  private token$ = new Subject<string>();
  private currentUser: UserTokenDto = null;
  private currentUserId: string;
  private currentToken = null;
  private isInstructor = false;

  constructor(
    private http: HttpClient,
  ) {

    const value = localStorage.getItem('currentUser');

    if (value != null) {
      this.currentUser = JSON.parse(value);

      const token = this.currentUser ? this.currentUser.token : '';
      this.currentToken = token;
    }

    this._toketProcess(this.currentUser);
  }

  private _toketProcess(tokenDto: UserTokenDto) {
    if (tokenDto != null) {
      try {
        const jwtPayload = jwt_decode(tokenDto.token);

        if (jwtPayload._id) {
          this.currentUserId = jwtPayload._id;
        } else {
          throw new Error(`ไม่พบ it บน token payload: ${jwtPayload}`);
        }

        if (jwtPayload.role) {
          this.isInstructor = jwtPayload.role === 'instructor';
        } else {
          throw new Error(`ไม่พบ rol บน token payload: ${jwtPayload}`);
        }

        localStorage.setItem('currentUser', JSON.stringify(tokenDto));

        this.login$.next(this.currentUser = tokenDto);
        this.token$.next(this.currentToken = this.currentUser ? this.currentUser.token : '');
      } catch (error) {
        console.error(`พบข้อผิดพลาดร้ายแรง เกี่ยวกับการจัดการ token: ${error}`);
      }
    } else {
      localStorage.removeItem('currentUser');

      this.login$.next(null);
      this.token$.next(null);
    }
  }

  getCurrentUserId(): string {
    return this.currentUserId;
  }

  get IsInstructor(): boolean {
    return this.isInstructor;
  }

  getCurrentUser(): UserTokenDto {
    return this.currentUser;
  }

  getCurrentToken(): string {
    return this.currentToken;
  }

  haveToken(): Observable<string> {
    return this.token$.asObservable();
  }

  isLogin(): Observable<UserTokenDto> {
    return this.login$.asObservable();
  }

  logout() {
    this._toketProcess(null);
  }

  // // for 1st stage, testing
  // mockLogin(username: string, password: string): void {
  //   localStorage.setItem('currentUser',
  //     JSON.stringify(this.currentUser = username)
  //   );
  //   this.login$.next(username);
  // }

  login(username: string, password: string) {
    // console.log(`${username}:${password}`);

    return this.http.post<UserTokenDto>(`/users/auth`,
      { username: username, password: password }
    ).pipe(
      // // จำลอง latency จาก MockBackend แทน
      // debounceTime(200),
      map(user => {

        console.log(user);
        if (user && user.username && user.token) {
          this._toketProcess(user);
        }
        return user;
      }),
    );
  }
}
