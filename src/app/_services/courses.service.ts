import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CourseDto } from '../_model/course-dto';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  private readonly _href = '/courses';
  constructor(
    private http: HttpClient
  ) { }

  // router.get('/', getAll);
  getAll$(): Observable<CourseDto> {
    return this.http.get<CourseDto>(this._href);
  }

  // router.get('/detail/:id', getById);
  getById$(courseId: string): Observable<CourseDto> {
    return this.http.get<CourseDto>(this._href.concat('/detail/').concat(encodeURI(courseId)));
  }

  // router.get('/registered', getRegisteredCourses);
  getRegisteredCourses$(): Observable<CourseDto> {
    return this.http.get<CourseDto>(this._href.concat('/registered'));
  }

  // router.get('/mind', getMyCourses);
  getMyCourses$(): Observable<CourseDto> {
    return this.http.get<CourseDto>(this._href.concat('/mind'));
  }

  // router.get('/name/:name', searchByName);
  searchByName$(courseName: string): Observable<CourseDto> {
    return this.http.get<CourseDto>(this._href.concat('/name/').concat(encodeURI(courseName)));
  }

  // router.get('/time/:time', searchByTime);
  searchByTime$(time: Date): Observable<CourseDto> {

    const imeJsonText = JSON.stringify(time);

    console.log(imeJsonText);

    return this.http.get<CourseDto>(this._href.concat('/time/').concat(encodeURI(imeJsonText)));
  }

  // router.post('/', creatCourse);
  creatCourse$(course: CourseDto): Observable<CourseDto> {
    return this.http.post<CourseDto>(this._href,
      course
    );
  }

  // router.put('/', updateCourse);
  updateCourse$(course: CourseDto): Observable<CourseDto> {
    return this.http.put<CourseDto>(this._href,
      course
    );
  }

  // // router.put('/registered', updateRegisterCourse);
  // updateRegisterCourse$(course: CourseDto): Observable<CourseDto> {
  //   return this.http.put<CourseDto>(this._href,
  //     course
  //   );
  // }

  // // router.put('/registered', updateRegisterCourse);
  enRoll$(courseId: string, studentId: string): any {
    return this.http.post(this._href.concat('/registered'),
      {
        'courseid': courseId,
        'studentid': studentId
      }
    );
  }
}
