import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegistrationDto } from '../_model/registration-dto';
import { Observable } from 'rxjs';
import { UserTokenDto } from '../_model/user-token-dto';
import { UserDto } from '../_model/user-dto';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpClient
  ) {

  }

  // getByUsername(username: string) {
  //   return this.http.get('/users/' + username);
  // }

  updateUser(user: UserTokenDto): Observable<any> {
    return this.http.put<UserDto>('/users',
      user
    );
  }

  register(user: RegistrationDto): Observable<UserTokenDto> {

    console.log(user);

    return this.http.post<UserTokenDto>('/users',
      user
    );
  }

  loadUser(): Observable<UserDto> {
    return this.http.get<UserDto>('/users');
  }
}
