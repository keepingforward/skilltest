import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { CourseMasterComponent } from './course/course-master.component';
import { CourseDetailComponent } from './course/course-detail.component';
import { CourseAddComponent } from './course/course-add.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full', data: { title: 'Skillane Course.' } },
  { path: 'register', component: RegisterComponent, data: { title: 'ลงทะเบียนสมัครสมาชิก' } },
  { path: 'login', component: LoginComponent, data: { title: 'ลงชื่อเข้าใช้ระบบ' } },
  { path: 'profile', component: ProfileComponent, data: { title: 'ข้อมูลส่วนตัว' } },
  { path: 'courses/add', component: CourseAddComponent, data: { title: 'เพิ่ม Course' } },
  { path: 'courses/detail/add', component: CourseDetailComponent, data: { title: 'เพิ่มบทเรียน' } },
  { path: 'courses/detail/:id', component: CourseDetailComponent, data: { title: 'รายละเอียดบทเรียน' } },
  {
    path: 'courses/:mode', component: CourseMasterComponent, data: { title: 'Skillane Course.' },
    children: [
      // { path: '', component: CourseMasterComponent, data: { title: 'Skillane Courses.' } },
      { path: 'all', component: CourseMasterComponent, data: { title: 'Skillane Courses ทั้งหมด.' } },
      { path: 'mind', component: CourseMasterComponent, data: { title: 'Courses ที่สอน' } },
      { path: 'registered', component: CourseMasterComponent, data: { title: 'Courses ที่ลงทะเบียนเรียน' } },
    ]
  },

  { path: '**', component: PageNotFoundComponent, data: { title: 'Found Error !!!' } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
