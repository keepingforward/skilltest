import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ServiceWorkerModule, SwUpdate } from '@angular/service-worker';
import { AppMaterialModule } from './_shared/app-material.module';
import { BrowserAnimationsModule } from '../../node_modules/@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './_services/authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RegisterComponent } from './register/register.component';
import { AppTitleService } from './_services/app-title.service';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        ServiceWorkerModule.register('/ngsw-worker.js', { enabled: false }),
        AppMaterialModule,
        HttpClientTestingModule,
      ],
      declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
      ],
      providers: [
        SwUpdate,
        AuthenticationService,
        AppTitleService,
      ]
    }).compileComponents();
  }), 10000);

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }), 10000);

  // it(`should have as title 'Skillane Course.'`, async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.debugElement.componentInstance;
  //   expect(app.title).toEqual('Skillane Course.');
  // }));

  // it('should render title in a .title tag', async(() => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('.title').textContent).toContain('Skillane Course.');
  // }));
});
