import {
  Component,
  ViewChild,
  NgZone,
  HostListener,
  OnInit,
  AfterViewInit,
  OnDestroy,
} from '@angular/core';
import {
  Router,
  ActivatedRoute,
  Event,
  NavigationEnd,
  ChildActivationEnd,
} from '@angular/router';
import {
  Subject,
  interval,
} from 'rxjs';
import {
  filter,
  debounceTime,
  map,
  mergeMap,
  takeUntil,
} from 'rxjs/operators';
import { SwUpdate } from '@angular/service-worker';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Title } from '@angular/platform-browser';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { AuthenticationService } from './_services/authentication.service';
import { AppTitleService } from './_services/app-title.service';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'cha-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  titleSuffix = ', by cha with love';
  title = 'Skillane Course.';
  isOffline = false;
  isLoggedIn = false;
  currentUserName = '';

  private readonly onDestroy$ = new Subject<void>();

  @ViewChild('sidenav', { static: false }) private _sidenav;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private readonly swUpdate: SwUpdate,
    private readonly zone: NgZone,
    private snackBar: MatSnackBar,
    private titleService: Title,
    private authenticationService: AuthenticationService,
    private appTitleService: AppTitleService,
  ) {
    // console.log(`this.router.url: ${this.router.url}`);
    this.router.events.pipe(
      // // map(e => {
      // //   console.log(e);
      // //   return e;
      // // }),
      // filter((event: Event) => event instanceof NavigationEnd),
      filter((event: Event) => event instanceof ChildActivationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data),
      map((data) => {
        if (data.title) {
          return data.title;
        } else {
          return this.router.url.split('/').reduce((acc, frag) => {
            // if (acc && frag) { acc += ' > '; }
            // console.log(acc);
            // console.log(frag);
            return frag;
            // return acc + TitleService.ucFirst(frag);
          });
        }
      }),
      debounceTime(150),
    ).subscribe(
      title => {
        this.setAppTitle(title);
        if (this._sidenav) {
          this._sidenav.opened = false;
        }
      }
    );
  }

  setAppTitle(newTitle: string): void {
    this.title = newTitle;
    this.setPageTitle(newTitle);
  }

  setPageTitle(newTitle: string): void {
    this.titleService.setTitle(newTitle.concat(this.titleSuffix));
  }

  ngAfterViewInit(): void {
    if (this.swUpdate.isEnabled) {
      const updateInterval = 5 * 60 * 1000; // 5 * 60 * 100; // 1000 * 60 * 60 * 6 // 6 Hours
      this.zone
        .runOutsideAngular(() =>
          interval(updateInterval)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe(() =>
              this.zone.run(() =>
                this.checkForUpdate()
              )
            )
        );
    }
  }

  ngOnDestroy(): void {
    if (this.onDestroy$) {
      this.onDestroy$.next();
      this.onDestroy$.complete();
      this.onDestroy$.unsubscribe();
    }
  }

  ngOnInit(): void {

    this.appTitleService.titleChanged()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((newTitle) => {
        this.setAppTitle(newTitle);
      });

    const userToken = this.authenticationService.getCurrentUser();
    this.currentUserName = (userToken != null) ? userToken.username : '';
    this.isLoggedIn = userToken != null;
    this.authenticationService.isLogin()
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((user) => {

        if (user) {
          this.currentUserName = user.username;
          this.isLoggedIn = true;
        } else {
          this.currentUserName = '';
          this.isLoggedIn = false;
        }
      });

    this.setAppTitle(this.title);

    if (this.swUpdate.isEnabled) {
      console.log(`swUpdate.isEnabled: ${this.swUpdate.isEnabled}`);
    }

    if (this.swUpdate.isEnabled) {
      this.swUpdate.activated.subscribe((event) => {
        console.log('swUpdate.activated');
        console.log('swUpdate.activated - current : ' + event.current.hash);
        console.log('swUpdate.activated - previous: ' + event.previous.hash);
        console.log();
      });

      this.swUpdate.available.subscribe((event) => {
        console.log('swUpdate.available');
        console.log('swUpdate.available - current  : ' + event.current.hash);
        console.log('swUpdate.available - available: ' + event.available.hash);
        console.log();

        this.swUpdate.activateUpdate().then(() => {
          const snack = this.snackBar.open('New Update Available !, Please reload app to see the latest.',
            'Reload',
            {
              duration: 60 * 1000
            });

          snack.onAction().subscribe(() => {
            window.location.reload();
          });
        });
      });
    }
  }

  checkForUpdate(): void {
    console.log('checkForUpdate');
    this.swUpdate.checkForUpdate();
  }

  @HostListener('window:online', [])
  onOnline() {
    this.isOffline = false;

    console.log('window:online');
  }

  @HostListener('window:offline', [])
  onOffline() {
    this.isOffline = true;

    console.log('window:offline');
  }

  signOut() {
    this.authenticationService.logout();
  }
}
