import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { BaseApiUrlInterceptor } from './_helpers/base-api-url.interceptor';
import { MAT_DATE_LOCALE } from '@angular/material/core';


export const appModuleProviders = {
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'th-TH' },
        { provide: HTTP_INTERCEPTORS, multi: true, useClass: JwtInterceptor },
        { provide: HTTP_INTERCEPTORS, multi: true, useClass: ErrorInterceptor },
        { provide: HTTP_INTERCEPTORS, multi: true, useClass: BaseApiUrlInterceptor },
    ]
};

