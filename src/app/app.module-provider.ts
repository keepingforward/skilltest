import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { BaseApiUrlInterceptor } from './_helpers/base-api-url.interceptor';
import { FakeBackendInterceptor } from './_helpers/fake-backend.interceptor';
import { MAT_DATE_LOCALE } from '@angular/material/core';

export const appModuleProviders = {
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'th-TH' },
        { provide: HTTP_INTERCEPTORS, multi: true, useClass: JwtInterceptor },
        { provide: HTTP_INTERCEPTORS, multi: true, useClass: ErrorInterceptor },
        { provide: HTTP_INTERCEPTORS, multi: true, useClass: BaseApiUrlInterceptor },

        // will be deleted, by app.moduel-provider.prod.ts file.
        // ให้ดูที่ angular.json 'fileReplacements'
        { provide: HTTP_INTERCEPTORS, multi: true, useClass: FakeBackendInterceptor },
    ]
};

