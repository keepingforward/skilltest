import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppMaterialModule } from './_shared/app-material.module';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegisterComponent } from './register/register.component';
import { appModuleProviders } from './app.module-provider';
import { ProfileComponent } from './profile/profile.component';
import { CourseListComponent } from './course/course-list.component';
import { CourseDetailComponent } from './course/course-detail.component';
import { CourseAllComponent } from './course/course-all.component';
import { CourseMasterComponent } from './course/course-master.component';
import { CourseAddComponent } from './course/course-add.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    PageNotFoundComponent,
    RegisterComponent,
    ProfileComponent,
    CourseListComponent,
    CourseDetailComponent,
    CourseAllComponent,
    CourseMasterComponent,
    CourseAddComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'chaApp' }),
    BrowserAnimationsModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AppMaterialModule,
    HttpClientModule,
  ],
  bootstrap: [
    AppComponent
  ],
  providers: appModuleProviders.providers,
})
export class AppModule { }
