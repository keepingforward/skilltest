import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { CoursesService } from '../_services/courses.service';
import { Observable, Subject, interval } from 'rxjs';
import { CourseDto } from '../_model/course-dto';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { tap, take, map } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
// import { UsersService } from '../_services/users.service';

@Component({
  selector: 'cha-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.scss']
})
export class CourseDetailComponent implements OnInit {
  form: FormGroup;
  canEdit = false;
  isAdding = false;
  isEditing = false;
  author = '';
  course$: Observable<CourseDto>;
  isReadonlyForm = true;
  isValidForm = false;
  id = null;
  errorMessage: string;
  haveError: boolean;
  isBusy = false;
  cachData: CourseDto;
  isEnroll: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthenticationService,
    private courseService: CoursesService,
  ) {
  }

  get f() { return this.form.controls; }

  ngOnInit() {

    const currentUser = this.authService.getCurrentUser();
    this.id = this.route.snapshot.paramMap.get('id');
    this.isAdding = false;

    console.log(this.id);

    if (this.id) {
      this.course$ = this.courseService.getById$(this.id) // '5b9a63bdcb64303b3461b572'
        .pipe(
          tap(course => {
            // this.canSummit = true;

            this.canEdit = false;

            console.log(course.author);

            if (course) {
              this.author = `${course.author.firstname} ${course.author.lastname}`;

              // console.log(`${course.author.username}:${currentUser.username}`);
              const username = currentUser ? currentUser.username : '';
              this.canEdit = (username === course.author.username);

              console.log(course);

              this.cachData = course;

              const userId = this.authService.getCurrentUserId();
              console.log(`userId: ${userId}`);
              console.log(course.students);

              if (course.students.indexOf(userId) > -1) {
                this.isEnroll = true;
              } else {
                this.isEnroll = false;
              }

              this.form.patchValue(course);
            }
          })
        );
    } else {
      this.isReadonlyForm = false;
      this.isAdding = true;

      const mockDto$ = new Subject<CourseDto>();

      console.log(currentUser);

      this.course$ = mockDto$.asObservable();
      const tmpData = {
        '_id': '',
        'students': [],
        'name': '',
        'author': {
          '_id': '',
          'username': '', // currentUser.username || '',
          'role': 'instructor'
        }
      };

      interval(300)
        .pipe(
          take(1),
          map(() => tmpData)
        ).subscribe((val) => {
          this.author = currentUser ? currentUser.username : '';
          mockDto$.next(val);
        });
    }

    this.form = new FormGroup(
      {
        'name': new FormControl({ value: '', disabled: this.isReadonlyForm }, [Validators.required]),
        'subject': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        'category': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        'description': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        'timestart': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        'timeend': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        // 'author': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        'numberofstudent': new FormControl({ value: '', disabled: true }, []),
      },
      (g) => {

        this.isValidForm = g.get('name').value.length > 0;

        return null;
      }
    );
  }

  CancelEdit() {
    // this.isEditing = false;

    // this.router.navigate([`/courses/detail/${this.id}`]);
    this.router.navigate([`/`]);
    // this.router.navigate(['/courses/detail/', this.id]);
  }

  EditMode() {
    this.isEditing = true;
    this.isReadonlyForm = false;

    // const cachData = this.form.value;
    this.form = new FormGroup(
      {
        'name': new FormControl({ value: '', disabled: this.isReadonlyForm }, [Validators.required]),
        'subject': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        'category': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        'description': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        'timestart': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        'timeend': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        // 'author': new FormControl({ value: '', disabled: this.isReadonlyForm }, []),
        'numberofstudent': new FormControl({ value: '', disabled: true }, []),
      },
      (g) => {

        this.isValidForm = g.get('name').value.length > 0;

        return null;
      }
    );
    this.form.patchValue(this.cachData);
  }

  CancelAdd() {
    // this.isAdding = false;

    // this.router.navigate(['/courses/detail/add']);
    this.router.navigate([`/`]);
  }

  AddSave() {
    if (!this.form.valid) {
      return;
    }

    this.courseService
      .creatCourse$(this.form.value)
      .subscribe(
        data => {
          console.log('updateUser: ');
          console.log(data);
          console.log();

          this.errorMessage = 'บันทึกข้อมูลเรียบร้อย';

          this.haveError = true;
          this.isBusy = false;
          this.isValidForm = false;
        },
        error => {

          console.log(error);

          this.errorMessage = `${error.error.message}`;
          this.haveError = true;
          this.isBusy = false;
        });
  }

  Enroll() {
    this.courseService
      .enRoll$(this.cachData._id, this.authService.getCurrentUserId())
      .subscribe(
        data => {

          this.errorMessage = 'ลงทะเบียนเรียนเรียบร้อย';

          this.haveError = true;
          this.isBusy = false;
          this.isValidForm = false;

          this.isEnroll = true;

          this.cachData.numberofstudent = this.cachData.numberofstudent + 1;
          this.form.patchValue(this.cachData);
        },
        error => {
          console.log(error);

          this.errorMessage = `${error.error.message}`;
          this.haveError = true;
          this.isBusy = false;
        }
      );
  }

  EditSave() {
    if (!this.form.valid) {
      return;
    }

    this.isBusy = true;

    Object.assign(this.cachData, this.form.value);

    this.courseService
      .updateCourse$(this.cachData)
      .subscribe(
        data => {
          console.log('updateUser: ');
          console.log(data);
          console.log();

          this.errorMessage = 'บันทึกข้อมูลเรียบร้อย';

          this.haveError = true;
          this.isBusy = false;
          this.isValidForm = false;
        },
        error => {
          console.log(error);

          this.errorMessage = `${error.error.message}`;
          this.haveError = true;
          this.isBusy = false;
        });
  }
}
