import { Component, OnInit, Input } from '@angular/core';
import { Observable, Subject, of, from, interval } from 'rxjs';
import { delay, take, debounceTime, map } from 'rxjs/operators';
import { CourseDto } from '../_model/course-dto';

@Component({
  selector: 'cha-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.scss']
})
export class CourseListComponent implements OnInit {

  constructor() { }

  @Input() courses$: Observable<CourseDto>;

  ngOnInit() {
  }

}
