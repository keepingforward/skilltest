import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseMasterComponent } from './course-master.component';
import { AppMaterialModule } from '../_shared/app-material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { CourseListComponent } from './course-list.component';
import { AuthenticationService } from '../_services/authentication.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CourseMasterComponent', () => {
  let component: CourseMasterComponent;
  let fixture: ComponentFixture<CourseMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        AppMaterialModule,
        HttpClientTestingModule,
      ],
      declarations: [
        CourseMasterComponent,
        CourseListComponent,
      ],
      providers: [
        AuthenticationService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
