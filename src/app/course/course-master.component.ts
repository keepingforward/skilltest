import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { AuthenticationService } from '../_services/authentication.service';
import { ActivatedRoute, Route, ParamMap } from '@angular/router';
import { switchMap, take, map, distinctUntilChanged, takeUntil, tap } from 'rxjs/operators';
import { Observable, of, Subject, interval } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { CoursesService } from '../_services/courses.service';
import { CourseDto } from '../_model/course-dto';

@Component({
  selector: 'cha-course-master',
  templateUrl: './course-master.component.html',
  styleUrls: ['./course-master.component.scss'],

  // Encapsulation has to be disabled in order for the
  // component style to apply to the select panel.
  encapsulation: ViewEncapsulation.None,
})
export class CourseMasterComponent implements OnInit, OnDestroy {

  private readonly onDestroy$ = new Subject<void>();

  panelSelectionType = new FormControl('name');
  selected = 'name';
  courseResp$: Observable<CourseDto>;

  searchForm: FormGroup;
  searchSubmit = new FormControl();
  canSummit = false;
  haveError = false;
  errorMessage = '';

  constructor(
    private route: ActivatedRoute,
    public authenticationService: AuthenticationService,
    public courseService: CoursesService,
  ) {
  }

  ngOnDestroy(): void {
    if (this.onDestroy$) {
      this.onDestroy$.next();
      this.onDestroy$.complete();
      this.onDestroy$.unsubscribe();
    }
  }
  get f() { return this.searchForm.controls; }

  ngOnInit() {

    this.searchForm = new FormGroup(
      {
        'panelSelectionType': new FormControl({ value: 'name' }, []),
        'searchText': new FormControl('', []),
        'searchTime': new FormControl('', []),
      },
      (g) => {
        const isName = (g.get('panelSelectionType').value === 'name');
        const tmpCtrl = isName
          ? g.get('searchText')
          : g.get('searchTime');

        this.canSummit = (tmpCtrl.value && (tmpCtrl.value.length > 0 || !isName));

        return null;
      }
    );

    // const mode = this.route.snapshot.paramMap.get('mode');
    // console.log(`params: ${mode} - by snapshot`);
    // this.selectedCourses$ = this.service.getHero(id);

    // this.route.data.subscribe(val => {
    //   console.log(val);
    // });

    // https://github.com/angular/angular/issues/18862
    this.route
      // .parent
      .paramMap
      .pipe(
        takeUntil(this.onDestroy$),
        // switchMap((params: ParamMap) => this.service.getHero(params.get('id')))
        // switchMap((params) => params.get('_mode')),
        // map(params => {
        //   console.log(`params: ${params.get('mode')}`);
        //   return params;
        // }),
        distinctUntilChanged()
      ).subscribe(params => {
        // console.log(`params: '${params.get('mode')}'`);

        const currentMode = `${params.get('mode')}`;

        if ('mind' === currentMode) {
          this.courseResp$ = this.courseService.getMyCourses$()
            .pipe(
              tap(() => { })
            );
        } else if ('registered' === currentMode) {
          this.courseResp$ = this.courseService.getRegisteredCourses$()
            .pipe(
              tap(() => { })
            );
        } else {
          this.courseResp$ = this.courseService.getAll$()
            .pipe(
              tap(() => { })
            );
        }
      });
  }

  onSubmit() {
    this.searchCourses(this.f.panelSelectionType.value);
  }

  searchCourses(searchType: string) {
    console.log(searchType);

    if (searchType === 'name') {
      this.courseResp$ = this.courseService.searchByName$(this.f.searchText.value)
        .pipe(
          tap(() => { })
        );
    } else {
      const tmpDate = new Date(this.f.searchTime.value);
      const projDate = new Date(tmpDate.getFullYear(), tmpDate.getMonth(), tmpDate.getDate());

      this.courseResp$ = this.courseService.searchByTime$(projDate)
        .pipe(
          tap(() => { })
        );
    }
  }
}
