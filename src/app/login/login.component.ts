import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthenticationService } from '../_services/authentication.service';
import { first } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { AppTitleService } from '../_services/app-title.service';
import { Router } from '@angular/router';

@Component({
  selector: 'cha-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isBusy = false;
  canSummit = false;
  haveError = false;
  errorMessage = '';
  hide = true;

  constructor(
    private authenticationService: AuthenticationService,
    private appTitleServer: AppTitleService,
    private route: Router,
  ) {

  }

  ngOnInit() {
    this.appTitleServer.setTitle('ลงชื่อเข้าใช้งาน');

    this.loginForm = new FormGroup(
      {
        'username': new FormControl('', [Validators.required]),
        'password': new FormControl('', [Validators.required]),
      },
      (g) => {
        this.canSummit = !(g.get('username').invalid || g.get('password').invalid);
        return this.canSummit ? null : { 'required': true };
      }
    );
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    if (this.loginForm.invalid) {
      return;
    }

    this.isBusy = true;
    this.haveError = false;
    this.errorMessage = '';

    // this.authenticationService.mockLogin(
    //   this.f.username.value,
    //   this.f.password.value,
    // );

    this.authenticationService.login(
      this.f.username.value,
      this.f.password.value,
    ).pipe(
      first()
    ).subscribe(
      data => {
        // ไม่ต้องทำอะไร, ให้ service จัดการแล้ว
        this.isBusy = false;
        this.haveError = false;
        this.errorMessage = '';

        if (this.route.url === '/login') {
          this.route.navigate(['/']);
        }
      },
      (error: HttpErrorResponse) => {
        this.isBusy = false;
        this.haveError = true;

        if (error.status === 0 || error.status === 405 || error.status === 504) {
          this.errorMessage = `${error.status}: ไม่สามารถติดต่อเซิร์ฟเวอร์ได้, กรุณาติดต่อผู้ดูแลระบบ`;
        } else if (error.status === 204 || error.status === 401) {
          this.errorMessage = 'ชื่อผู้ใช้งาน หรือรหัสผ่านผิด';
        } else {
          // กรณีนี้ไม่ปกติ
          if (error.error) {
            this.errorMessage = `พบข้อผิดพลาด: ${error.error.message}`;
          } else {
            this.errorMessage = `พบข้อผิดพลาด: ${error.status}`;
          }
        }
      }
    );
  }

}
