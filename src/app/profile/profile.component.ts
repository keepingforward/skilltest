import { Component, OnInit } from '@angular/core';
import { UsersService } from '../_services/users.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { UserDto } from '../_model/user-dto';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'cha-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  form: FormGroup;
  user: Observable<UserDto>;
  haveError = false;
  isBusy = false;
  errorMessage = '';
  canSummit: true;

  constructor(
    // private formBuilder: FormBuilder,
    private userService: UsersService,
  ) { }

  ngOnInit() {
    this.form = new FormGroup(
      {
        'role': new FormControl({ value: 'student', disabled: true }, [Validators.required]),
        'username': new FormControl({ value: '', disabled: true }, [Validators.required]),
        'firstname': new FormControl('', []),
        'lastname': new FormControl('', []),
        'nickname': new FormControl('', []),
        'gender': new FormControl('', []),
        'birthday': new FormControl('', []),
      },
      // (g) => {

      //   console.log(this.canSummit = g.get('birthday').value);
      //   // this.canSummit = g.get('birthday').value.match(/^[1-9]?[0-9]\/[1-9]?[0-9]\/[0-9]{4}$/);

      //   // return this.canSummit ? null : { 'invalidformat': true };

      //   return null;
      // }
    );

    // this.user = this.userService.loadUser();
    this.user = this.userService.loadUser()
      .pipe(
        tap(user => {
          this.canSummit = true;
          this.form.patchValue(user);
        })
      );
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    this.errorMessage = '';
    this.haveError = false;
    this.isBusy = true;

    console.log(this.form.value);
    this.userService
      .updateUser(this.form.value)
      .subscribe(
        data => {
          console.log('updateUser: ');
          console.log(data);
          console.log();

          // this.alertService.success('Registration successful', true);

          this.errorMessage = 'บันทึกข้อมูลเรียบร้อย';
          this.haveError = true;

          this.isBusy = false;

        },
        error => {
          // this.alertService.error(error);
          // this.loading = false;

          console.log(error);

          this.errorMessage = `${error.error.message}`;
          this.haveError = true;
          this.isBusy = false;
        });
  }
}
