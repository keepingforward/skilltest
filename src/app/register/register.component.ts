import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppTitleService } from '../_services/app-title.service';
import { UsersService } from '../_services/users.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'cha-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registrationForm: FormGroup;
  canSummit = false;
  hide = true;
  passwordMached = true;
  isBusy = false;
  haveError = false;
  errorMessage = '';

  constructor(
    private appTitleServer: AppTitleService,
    private userService: UsersService,
    private router: Router,
  ) { }

  get f() { return this.registrationForm.controls; }

  ngOnInit() {
    this.registrationForm = new FormGroup(
      {
        'username': new FormControl('', [Validators.required]),
        'password': new FormControl('', [Validators.required]),
        'password2': new FormControl('', []),
        'firstname': new FormControl('', []),
        'lastname': new FormControl('', []),
        'nickname': new FormControl('', []),
        'birthday': new FormControl('', []),
        'gender': new FormControl({ value: '' }, []),
        'role': new FormControl({ value: 'student' }, [Validators.required]),
      },
      (g) => {
        this.passwordMached = (g.get('password').value === g.get('password2').value);
        if (!this.passwordMached) {
          g.get('password2').setErrors({ matchPassword: true });
        }

        this.canSummit = !(g.get('username').invalid || g.get('password').invalid)
          && this.passwordMached;

        return this.canSummit ? null : { 'required': true };
      }
    );

    this.f.role.setValue('student');
    this.f.gender.setValue('');

    // this.parent.setAppTitle('ลงทะเบียนสมัครสมาชิก');
    this.appTitleServer.setTitle('ลงทะเบียนสมัครสมาชิก');
  }

  onSubmit() {
    // console.log(this.registrationForm.invalid);

    if (this.registrationForm.invalid) {
      return;
    }

    this.isBusy = true;
    this.haveError = false;
    this.errorMessage = '';

    // console.log(this.registrationForm.value);
    this.userService.register(this.registrationForm.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log('register: ');
          console.log(data);
          console.log();

          // this.alertService.success('Registration successful', true);
          this.router.navigate(['/login']);
        },
        error => {
          // this.alertService.error(error);
          // this.loading = false;

          console.log(error);

          this.errorMessage = `${error.error.message}`;
          this.haveError = true;
          this.isBusy = false;
        });
  }

}
