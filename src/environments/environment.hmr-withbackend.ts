const proxy = require('../../dev-proxy.conf.json');

export const environment = {
  production: false,
  hmr: true,
  // apiUrl: '/api/v1', // not allowed slash on trail.
  // apiUrl: '//127.0.0.1:4220/api/v1',
  apiUrl: '//192.168.137.28:4220/api/v1',
  // apiUrl:: 'http://192.168.137.28:4220',
};
