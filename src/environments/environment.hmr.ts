export const environment = {
  production: false,
  hmr: true,
  apiUrl: '/api/v1', // not allowed slash on trail.
  // apiUrl:: 'http://192.168.137.28:4220',
};
