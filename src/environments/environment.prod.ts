export const environment = {
  production: true,
  hmr: false,
  apiUrl: '/api/v1', // not allowed slash on trail.
  // apiUrl: '//172.17.0.4:4220/api/v1'
  // apiUrl: '//127.0.0.1:4220/api/v1', // not allowed slash on trail.
  // apiUrl:: '/api/v1',
};
