// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-firefox-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma'),
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, '../coverage'),
      reports: ['html', 'lcovonly'],
      fixWebpackSourcePaths: true
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    singleRun: false,
    concurrency: 1,
    browsers: ['Chrome', 'ChromeCI', 'Firefox', 'FirefoxCI', 'FirefoxHeadless'],
    customLaunchers: {
      ChromeCI: {
        base: 'Chrome',
        displayName: 'Chrome headless and no-sandbox',
        flags: [
          // https://developers.google.com/web/updates/2017/04/headless-chrome
          // http://cvuorinen.net/2017/05/running-angular-tests-in-headless-chrome/
          '--headless', //-- fails, when run test 'ng test'          
          '--remote-debugging-port=9222', // When using --headless without a remote debugging port, Google Chrome exits immediately.
          '--no-sandbox',
          '--no-proxy-server',
          '--disable-gpu',
          '--user-data-dir',
          '--disable-extensions',
          '--disable-web-security',
          '--lang=en_US',
          // '--test-type=browser'
        ]
      },
      FirefoxCI: {
        base: 'Firefox',
        displayName: 'Firefox -safe-mode ___________',
        flags: [
          // '-headless', // Headless Firefox works on Fx55+ on Linux, and 56+ on Windows/Mac. // https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Headless_mode#Browser_support
          '-safe-mode '
        ]
      },
      FirefoxHeadless: {
        base: 'Firefox',
        displayName: 'Firefox -headless -safe-mode _',
        flags: [
          '-headless', // Headless Firefox works on Fx55+ on Linux, and 56+ on Windows/Mac. // https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Headless_mode#Browser_support
          '-safe-mode '
        ]
      },
    },
  });
};
